import org.junit.jupiter.api.Test;

import com.kor.readingroom.Configuration.HibernateConfig;
import org.testng.Assert;

public class ConfigTest {

    @Test
    public void configureBD() { // Тест проверяющий правильно ли работает конфигурация hibernate.cfg.xml
        Assert.assertNotNull(HibernateConfig.em,
                "Менеджер сущностей пустой, произошла ошибка, проверьте конфигурацию подключения к БД");
    }
}
