module com.kor.readingroom {

    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires java.logging;

    requires java.naming;
    requires java.persistence;
    requires java.sql;
    requires org.hibernate.orm.core;


    opens com.kor.readingroom to javafx.fxml;
    exports com.kor.readingroom;
    exports com.kor.readingroom.Controllers;
    opens com.kor.readingroom.Controllers to javafx.fxml;
    opens com.kor.readingroom.Configuration;
    opens com.kor.readingroom.Databases;
    exports com.kor.readingroom.Databases;
}