package com.kor.readingroom;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("LaunchScreen"), 400,600);
        stage.setTitle("Читальный зал");
        stage.setScene(scene);
        stage.setResizable(true);
        stage.show();
    }

    public static Parent getRoot() {
        return scene.getRoot();
      }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(
                fxml + ".fxml"

        ));
        return fxmlLoader.load();
    }
}