package com.kor.readingroom.Configuration;

import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class HibernateConfig {

    // https://www.tutorialspoint.com/hibernate/hibernate_examples.htm - один из примеров конфигурации сессии

    static StandardServiceRegistry registry = new StandardServiceRegistryBuilder() // Создаём экземпляр класса Менеджер сущностей
            .configure() // Берём конфигурацию hibernate.cfg.xml
            .build(); // Строим подключение

    public static final EntityManagerFactory emf = new MetadataSources(registry) // Создаём экземпляр класса Менеджер сущностей
            .buildMetadata() // Собираем метаданные
            .buildSessionFactory(); // Создаем фабрику для сессий

    public static EntityManager em = emf.createEntityManager(); // Создаём экземпляр класса Менеджер сущностей
}
