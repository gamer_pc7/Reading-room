package com.kor.readingroom.Controllers;

import com.kor.readingroom.Configuration.HibernateConfig;
import com.kor.readingroom.Databases.Book;
import com.kor.readingroom.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.io.IOException;
import java.util.List;

public class SelectRoom {

    public void doRoom1(ActionEvent actionEvent) throws IOException { // Переход на страницу выбора места в 1 зале
        Main.setRoot("Room1Mesto");
    }

    public void doRoom2(ActionEvent actionEvent) { // Переход на страницу выбора места в 2 зале

    }

    public void doUserScreen(ActionEvent actionEvent) throws IOException { // Вернуться на страницу пользователя
        Main.setRoot("UserScreen");
    }
}
