package com.kor.readingroom.Controllers;

import com.kor.readingroom.Configuration.HibernateConfig;
import com.kor.readingroom.Databases.Role;
import com.kor.readingroom.Main;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import javax.persistence.Query;
import java.io.IOException;

public class RegistrationScreen {

    public TextField Email;
    public PasswordField Password;
    public TextField Login;
    public TextField Phone;
    public TextField FIO;

    public void DoRegistration(ActionEvent actionEvent) throws IOException {

        if (Email.getText().isBlank() || Password.getText().isBlank() || Login.getText().isBlank() || Phone.getText().isBlank() || FIO.getText().isBlank()) { // Проверка на пустые поля
            Alert error = new Alert(Alert.AlertType.ERROR); // Создаём экземпляр класса Алерт

            error.setTitle("Поля не должны быть пустыми");
            error.setHeaderText("Ошибка");
            error.setContentText("Заполните поля");

            error.showAndWait(); // Показываем и ждём
        } else {
            try {

                String hql = "INSERT INTO `user`(`FIO`, `number`, `login`, `password`, `email`)" +
                        "VALUES ('" + FIO.getText() + "','" + Phone.getText() + "','" + Login.getText() + "','" + Password.getText() + "','" + Email.getText() + "')";


                Query query = HibernateConfig.em.createNativeQuery(hql);

                HibernateConfig.em.getTransaction().begin();
                query.executeUpdate(); // Пробуем создать пользователя. Login в БД указан как уникальный
                HibernateConfig.em.getTransaction().commit();
                Main.setRoot("LaunchScreen");
            } catch (Exception e) { // Если поймали исключение сообщаем пользователю об этом
                e.printStackTrace();
                Alert error = new Alert(Alert.AlertType.ERROR); // Создаём экземпляр класса Алерт

                error.setTitle("Логин занят");
                error.setHeaderText("Ошибка");
                error.setContentText("Попробуйте другой логин");

                error.showAndWait(); // Показываем и ждём
            }
        }
    }
}
