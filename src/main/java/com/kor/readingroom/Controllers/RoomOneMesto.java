package com.kor.readingroom.Controllers;

import com.kor.readingroom.Main;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;

import java.io.IOException;

public class RoomOneMesto {

    /*
    Миллиард методов под каждое место
     */

    public void place18(ActionEvent actionEvent) {
        goRoom(18);
    }

    public void place19(ActionEvent actionEvent) {
        goRoom(19);
    }

    public void place20(ActionEvent actionEvent) {
        goRoom(20);
    }

    public void place1(ActionEvent actionEvent) {
        goRoom(1);
    }

    public void place2(ActionEvent actionEvent) {
        goRoom(2);
    }

    public void place3(ActionEvent actionEvent) {
        goRoom(3);
    }

    public void place4(ActionEvent actionEvent) {
        goRoom(4);
    }

    public void place5(ActionEvent actionEvent) {
        goRoom(5);
    }

    public void place6(ActionEvent actionEvent) {
        goRoom(6);
    }

    public void place7(ActionEvent actionEvent) {
        goRoom(7);
    }

    public void place9(ActionEvent actionEvent) {
        goRoom(9);
    }

    public void place11(ActionEvent actionEvent) {
        goRoom(11);
    }

    public void place13(ActionEvent actionEvent) {
        goRoom(13);
    }

    public void place8(ActionEvent actionEvent) {
        goRoom(8);
    }

    public void place10(ActionEvent actionEvent) {
        goRoom(10);
    }

    public void place12(ActionEvent actionEvent) {
        goRoom(12);
    }

    public void place14(ActionEvent actionEvent) {
        goRoom(14);
    }

    public void place15(ActionEvent actionEvent) {
        goRoom(15);
    }

    public void place16(ActionEvent actionEvent) {
        goRoom(16);
    }

    public void place17(ActionEvent actionEvent) {
        goRoom(17);
    }

    public void goRoom(int number){ // Метод принимает int
        Alert error = new Alert(Alert.AlertType.INFORMATION); // Создаём экземпляр класса Алерт

        error.setTitle("Успешно");
        error.setHeaderText("Вы выбрали место #" + number);
        error.setContentText("Проходите в зал для чтения, там вас будет ждать администратор");

        error.showAndWait(); // Показываем и ждём

        try {
            Main.setRoot("LaunchScreen");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void doSelectRoom(ActionEvent actionEvent) throws IOException { // Вернуться обратно на экран выбора зала
        Main.setRoot("SelectRoom");
    }

    public static void initialize() { // Метод запускается после загрузке окна
        /*
        Добавить метод который проверяет какие места в зале заняты
        и блокировать выбор их в приложении
         */
    }
}
