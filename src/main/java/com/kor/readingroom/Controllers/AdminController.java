package com.kor.readingroom.Controllers;

import com.kor.readingroom.Main;
import javafx.event.ActionEvent;

import java.io.IOException;

public class AdminController {

    public void doLoginScreen(ActionEvent actionEvent) throws IOException {
        Main.setRoot("LaunchScreen");
    }

    public void getEmployee(ActionEvent actionEvent) throws IOException {
        Main.setRoot("GetEmployee");
    }

    public void getClients(ActionEvent actionEvent) throws IOException {
        Main.setRoot("GetClients");
    }
}
