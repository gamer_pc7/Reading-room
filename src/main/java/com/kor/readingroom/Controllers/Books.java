package com.kor.readingroom.Controllers;

import com.kor.readingroom.Configuration.HibernateConfig;
import com.kor.readingroom.Databases.Book;
import com.kor.readingroom.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.IOException;
import java.util.List;

public class Books {

    public TableView BookTable;
    public TableColumn<Book, String> author, name, year;

    @FXML
    public void initialize(){ // Метод запускается с откртием окна и собирает информацию о доступной литературе

        CriteriaBuilder builder = HibernateConfig.em.getCriteriaBuilder();
        CriteriaQuery<Book> criteria = builder.createQuery(Book.class);
        criteria.from(Book.class);
        List<Book> books = HibernateConfig.em.createQuery(criteria).getResultList();

//        for (Book book : books) {
//            System.out.println(book.getName()); Использовалась для отладки вывода информации для книг
//        }

        ObservableList<Book> bookObservableList = FXCollections.observableList(books);
        BookTable.setItems(bookObservableList);
    }

    public void doUserScreen(ActionEvent actionEvent) throws IOException { // Вернуться на страницу пользователя
        Main.setRoot("UserScreen");
    }

}
