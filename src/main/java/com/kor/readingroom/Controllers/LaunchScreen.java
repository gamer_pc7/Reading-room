package com.kor.readingroom.Controllers;

import com.kor.readingroom.Configuration.HibernateConfig;

import com.kor.readingroom.Databases.User;
import com.kor.readingroom.Main;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.IOException;

public class LaunchScreen {

    // Поля хранящие в себе значения логина и пароля
    public TextField LoginField;
    public PasswordField PasswordField;

    public void DoLogin(ActionEvent actionEvent) throws IOException { // Вход в систему

        // Запрос для нахождения пользователя в БД
        Query q = HibernateConfig.em.createNamedQuery("user.findByLogin");
        q.setParameter("login", LoginField.getText());


        try { // Выполняем запрос к БД и ловим ошибки
            User u = (User) q.getSingleResult();

            if (u.getPassword().equals(PasswordField.getText())) { // Проверка пароля
                switch (u.getRole().getName()) {
                    case "User" -> // В 1м случае вызывается обычное окно
                            Main.setRoot("UserScreen");
                    case "Admin" -> // В 2м случае вызывается окно администратора
                            Main.setRoot("AdminScreen");
                    default -> // Если у пользователя нет роли, выдаём ему алерт на экран
                            alert("Помощь", "Ошибка авторизации", "Обратитесь к администратору");
                }
            }

        } catch (Exception a) { // Если поймаем ошибку на моменте поиска пользователя в БД, вызывается это решение ошибки
            alert("Помощь", "Неверный логин или пароль", "Проверьте данные входа");
        }
    }

    public void DoRegistration(ActionEvent actionEvent) throws IOException { // Переход в окно регистрации
        Main.setRoot("Registration");
    }


    public void helpPassword(MouseEvent mouseEvent) { // Метод для информирования по проблеме забытого пароля
        alert("Помощь", "Забыли пароль?", "Обратитесь к администратору");
    }

    public void alert(String title, String header, String context) {
        Alert error = new Alert(Alert.AlertType.ERROR); // Создаём экземпляр класса Алерт

        error.setTitle(title);
        error.setHeaderText(header);
        error.setContentText(context);

        error.showAndWait(); // Показываем и ждём
    }

}
