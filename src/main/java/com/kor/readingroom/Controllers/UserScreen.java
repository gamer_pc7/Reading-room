package com.kor.readingroom.Controllers;

import com.kor.readingroom.Main;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import javax.persistence.Query;
import java.io.IOException;

public class UserScreen {

    public void enterRoom(ActionEvent actionEvent) throws IOException { // Переход на экран выбора зала
        Main.setRoot("SelectRoom");
    }

    public void checkBooks(ActionEvent actionEvent) throws IOException { // Просмотреть список книг от лица клиента
        Main.setRoot("GetBooks");
    }

    public void exit(ActionEvent actionEvent) throws IOException { // Выйти на стартовый экран
        Main.setRoot("LaunchScreen");
    }
}
